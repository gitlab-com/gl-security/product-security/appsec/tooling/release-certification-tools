require 'httparty'
require_relative 'constants'

class MergeMonitor
  def self.run
    unreviewed_merge_requests = check_merged
    if unreviewed_merge_requests.length > 0
      create_issue(unreviewed_merge_requests)
    else
      puts "Complete! No merged JiHu MRs missing the #{SECURITY_REVIEW_COMPLETE_LABEL_NAME} label found."
    end
  end

  def self.check_merged
    puts "Checking for merge requests...\n======"

    unreviewed_merge_requests = []
    known_merge_requests = get_known_merge_requests

    ALL_PROJECT_IDS.each do |id|
      puts "Checking #{PROJECT_NAME_FOR[id]}..."
      url = "#{API_URL}/projects/#{id}/merge_requests"
      response = HTTParty.get(url, headers: request_headers, query: request_params)
      puts "...found #{response.length} merged MRs without the #{SECURITY_REVIEW_COMPLETE_LABEL_NAME} label in #{PROJECT_NAME_FOR[id]}\n"
      response.each do |merge_request|
        if known_merge_requests.include?(merge_request['id'])
          puts "...#{merge_request['title']} is known and already reported"
        else
          unreviewed_merge_requests << merge_request
        end
      end
      puts "======"
    end

    unreviewed_merge_requests
  end

  def self.request_headers
    HEADERS.merge({
      'Content-Type'  => 'application/json'
    })
  end

  def self.request_params
    {
      'state' => 'merged',
      'labels' => 'JiHu contribution',
      'not' => {
        'labels' => [SECURITY_REVIEW_COMPLETE_LABEL_NAME]
      }
    }
  end

  def self.create_issue(unreviewed_merge_requests)
    puts "Results found, creating issue..."
    issue_request_body = create_request_body(unreviewed_merge_requests).to_json
    url = "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues"
    response = HTTParty.post(url, headers: request_headers, body: issue_request_body).parsed_response
    puts "...Issue created at #{response['web_url']}"
  end

  def self.create_request_body(unreviewed_merge_requests)
    description = generate_description(unreviewed_merge_requests)
    {
      'title' => "Merge Monitor Report for #{Time.now.strftime('%Y-%m-%d')}",
      'description' => description
    }
  end

  def self.generate_description(unreviewed_merge_requests)
    description = ""
    description = "## Merged JiHu MRs without the #{SECURITY_REVIEW_COMPLETE_LABEL_NAME} label\n"
    description << "Please follow the [JiHu Contribution Merge Monitor Reports runbook](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/jihu-contribution-merge-monitor-reports.html) process.\n"
    description << '/label ~"AppSecWorkType::FedAppSecRelCert"' + " \n\n"

    unreviewed_merge_requests.each do |merge_request|
      repository = PROJECT_NAME_FOR[merge_request['project_id'].to_s]
      mr_title = merge_request['title']
      mr_url = merge_request['web_url']

      description << "* (#{repository}) [#{mr_title}](#{mr_url})\n"
      description << "  * - [ ] MR reviewed\n"
      description << "  * - [ ] Label applied\n"
      description << "  * - [ ] Summary: (Edit and write summary here)\n\n"
    end

    description << "cc #{FEDERAL_APPSEC_TEAM_ALIAS}\n\n"
    description
  end

  def self.get_known_merge_requests
    report_issues_url = "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues?state=opened"
    issues_response = HTTParty.get(report_issues_url, headers: request_headers)
    if issues_response.code == 401
      puts "The personal access token doesn't have access to the required project, ensure a proper PAT is set."
      exit 1
    end
    return [] if issues_response.empty?

    known_merge_request_ids = []

    issues_response.each do |issue|
      related_merge_requests_url = "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues/#{issue['iid']}/related_merge_requests"
      related_merge_requests = HTTParty.get(related_merge_requests_url, headers: request_headers)
      merge_request_ids = related_merge_requests.map { |merge_request| merge_request['id'] }
      known_merge_request_ids << merge_request_ids
    end

    known_merge_request_ids.flatten.uniq
  end
end
