require 'httparty'

require_relative 'constants'
require_relative 'utils'

class ChecklistGenerator
  def self.create_checklist(options)
    puts 'Creating checklist...'
    release_task = find_release_task(options)
    jihu_contributions = Utils.identify_jihu_contributions_from_blog_diff(release_task['iid'])
    create_release_certification_issue(release_task, jihu_contributions)
  end

  def self.find_release_task(options)
    if options[:task_id]
      Utils.retrieve_specific_task_issue(options[:task_id])
    else
      find_current_release_task
    end
  end

  def self.find_current_release_task
    puts 'finding current release task...'
    url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues?labels=Monthly%20Release&state=opened&search=Release&in=title&not[labels]=security"
    response = HTTParty.get(url, headers: HEADERS).parsed_response
    monthly_release_task_issue = response.detect { |r| r['description'].include?("22nd: release day") }
    raise 'release task not found' if monthly_release_task_issue.nil?
    raise 'unable to identify release task issue: issue title does not match' unless release_task_title_matches?(monthly_release_task_issue['title'])
    puts "...done. Found #{monthly_release_task_issue['title']} at #{monthly_release_task_issue['web_url']}"
    monthly_release_task_issue
  end

  def self.release_task_title_matches?(title)
    title.match?(/^Release \d{1,}.\d{1,}$/)
  end

  def self.retrieve_jihu_contributions_for(milestone_title)
    puts 'Retrieving JiHu Contributions for all projects...'
    jihu_contributions_for_projects = {}
    ALL_PROJECT_IDS.each do |project_id|
      response = request_jihu_contributions_for_project(project_id, milestone_title)
      jihu_contributions_for_projects[project_id] = response
    end
    contribution_count = determine_contribution_count(jihu_contributions_for_projects)
    puts "...done. Found #{contribution_count} JiHu Contributions."
    jihu_contributions_for_projects
  end

  def self.determine_contribution_count(jihu_contributions)
    jihu_contributions.values.flatten.length
  end

  def self.request_jihu_contributions_for_project(id, milestone_title)
    url = "#{API_URL}/projects/#{id}/merge_requests?labels=#{JIHU_LABEL_NAME}&milestone=#{milestone_title}&state=merged"
    HTTParty.get(url, headers: HEADERS).parsed_response
  end

  def self.create_release_certification_issue(release_task, jihu_contributions)
    current_user = nil #Utils.retrieve_current_user
    url = "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues"
    issue_description = build_description(jihu_contributions, release_task['web_url'])
    body = Utils.create_release_certification_issue_params(release_task, issue_description, current_user).to_json
    response = HTTParty.post(url, headers: HEADERS.merge({'Content-Type' => 'application/json'}), body: body)
    certification_issue_url = response.parsed_response['web_url']
    puts "Issue created at #{certification_issue_url}. Finalizing issue description..."
    Utils.append_data_to_issue_description(response.parsed_response['iid'], certification_issue_url, issue_description)
    puts '...done. All steps complete!'
  end

  def self.build_description(jihu_contributions, release_task_url)
    description = Utils.generate_certification_checklist_issue_header(release_task_url)
    description << "#{CONTRIBUTIONS_LIST_MESSAGE}\n\n"

    ALL_PROJECT_IDS.each do |project_id|
      contributions = jihu_contributions[project_id]
      description << "### #{PROJECT_DISPLAY_NAME_FOR[project_id]}\n"
      if contributions.length < 1
        description << "#{NO_CONTRIBUTIONS_MESSAGE}\n\n"
      else
        contributions.each do |c|
          description << Utils.build_contribution_display(c)
        end
      end
    end

    description << "#{MANUAL_REVIEW_HEADER}\n\n"
    description << "#{MANUAL_REVIEW_DESCRIPTION}\n\n"
    description << "#{MANUAL_REVIEW_INSTRUCTIONS}\n\n"
    PRIVATE_PROJECT_IDS.each do |project_id|
      description << "### #{PROJECT_DISPLAY_NAME_FOR[project_id]}\n"
      description << "**(Note: #{PROJECT_NOTE_FOR[project_id]})**\n" if PROJECT_NOTE_FOR[project_id]
      description << "#{MANUAL_REVIEW_DEFAULT_CHECKBOX}\n"
      description << "#{MANUAL_REVIEW_NO_MERGE_REQUESTS_CHECKBOX}\n\n"
    end
    description
  end
end
