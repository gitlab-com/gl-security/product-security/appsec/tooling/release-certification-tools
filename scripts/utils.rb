require 'httparty'
require_relative 'constants'

class Utils
  def self.retrieve_current_user
    url = "#{API_URL}/user"
    HTTParty.get(url, headers: HEADERS).parsed_response
  end

  def self.generate_certification_checklist_issue_header(release_task_url)
    description = ""
    description << "## AppSec Release Certification\n"
    description << "This is the AppSec release certification checklist. Once finished, be sure to put the certification comment on the [release task issue](#{release_task_url}). #{DOCUMENTATION_MESSAGE}\n\n"
    description << "cc #{FEDERAL_APPSEC_TEAM_ALIAS}\n\n"
    description << '/label ~"AppSecWorkType::FedAppSecRelCert"' + " \n\n"
    description << "## JiHu Contributions\n"
  end

  def self.create_release_certification_issue_params(release_task, issue_description, current_user)
    {
      'title' => "AppSec Release Certification for #{release_task['title']}",
      'milestone_id' => release_task.dig('milestone', 'id'),
      'description' => issue_description
    }
  end

  def self.append_data_to_issue_description(issue_iid, certification_issue_url, issue_description)
    updated_description = update_issue_description(issue_description, certification_issue_url)
    url = "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues/#{issue_iid}"
    HTTParty.put(url, headers: HEADERS.merge({'Content-Type' => 'application/json'}), body: { 'description' => updated_description }.to_json)
  end

  def self.update_issue_description(issue_description, certification_issue_url)
    new_description = ''
    new_description << issue_description
    new_description << "\n## Certification Comment\nThis comment certifies that no new significant vulnerabilities have been identified in this release. This release has been certified through [a review](#{certification_issue_url}) as part of the [release certification process](#{RELEASE_CERTIFICATION_DOCUMENTATION_URL})."
  end

  def self.retrieve_specific_task_issue(task_id)
    puts 'retrieving release task...'
    url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues/#{task_id}"
    HTTParty.get(url, headers: HEADERS).parsed_response
  end

  def self.generate_label_display(security_review_label_added)
    if security_review_label_added
      "~\"#{SECURITY_REVIEW_COMPLETE_LABEL_NAME}\""
    else
      ':x: **security review label not present**'
    end
  end

  def self.build_contribution_display(contribution)
    security_review_label_added = contribution['labels'].include?(SECURITY_REVIEW_COMPLETE_LABEL_NAME)
    label_display = generate_label_display(security_review_label_added)
    if security_review_label_added
      "- [x] (#{contribution['state']}) [#{contribution['title']}](#{contribution['web_url']}) **|** #{label_display}\n"
    else
      "- [ ] (#{contribution['state']}) [#{contribution['title']}](#{contribution['web_url']}) **|** #{label_display}\n"
    end
  end

  def self.title_matches_patch_release(issue_title)
    issue_title.match(/\d+.\d+.\d+/)
  end
  def self.mr_has_jihu_label(mr)
    mr['labels'].each do |label|
      return true if label.include?(CGI.unescape(JIHU_LABEL_NAME))
    end
    false
  end
  def self.paginated_http_get(url, headers)
    all_items = []
    page = 1

    loop do
      full_url = "#{url}?page=#{page}"
      response = HTTParty.get(full_url, headers: headers)
      raise "Problem while fetching the API at #{full_url}, received #{response.code} status code." unless response.success?

      items = response.parsed_response
      all_items.concat(items)

      break unless response.headers['X-Next-Page'].to_i > page
      page += 1
    end

    all_items
  end
  def self.identify_jihu_contributions_from_blog_diff(task_id)
    jihu_contribution_mrs = {}
    ALL_PROJECT_IDS.each do |project_id|
      jihu_contribution_mrs[project_id] = []
    end

    url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues/#{task_id}/related_merge_requests"
    related_merge_requests = paginated_http_get(url, HEADERS)

    related_merge_requests.each do |related_mr|
      next unless related_mr['title'].match?(/blog/i)

      diffs_url = "#{API_URL}/projects/#{WWW_GITLAB_COM_PROJECT_ID}/merge_requests/#{related_mr['iid']}/diffs"
      diffs_response = paginated_http_get(diffs_url, HEADERS)
      next unless diffs_response&.any?

      latest_diff = diffs_response.last
      url_link_regex = /\bhttps?:\/\/\S+/i
      matches = latest_diff['diff'].scan(url_link_regex)

      matches.each do |url|
        next unless url.include?('merge_requests')

        iid = url.scan(/\/merge_requests\/(\d+)/).flatten.first
        project_id = extract_project_id_from_url(url)

        next unless project_id && jihu_contribution_mrs.keys.include?(project_id)

        process_merge_request(project_id, iid, jihu_contribution_mrs)
      end
    end
    jihu_contribution_mrs
    end
  def self.extract_project_id_from_url(url)
    case
    when url.include?('gitlab-org/gitlab')
      GITLAB_PROJECT_ID
    when url.include?('gitlab-org/omnibus-gitlab')
      OMNIBUS_PROJECT_ID
    when url.include?('gitlab-org/build/CNG')
      CNG_PROJECT_ID
    when url.include?('gitlab-org/gitaly')
      GITALY_PROJECT_ID
    end
  end

  def self.process_merge_request(project_id, iid, jihu_contribution_mrs)
    url = "#{API_URL}/projects/#{project_id}/merge_requests/#{iid}"
    response = HTTParty.get(url, headers: HEADERS)
    raise "Error while fetching merge request #{url}, received #{response.code} " unless response.success?

    blog_mr = response.parsed_response
    jihu_contribution_mrs[project_id] << blog_mr if mr_has_jihu_label(blog_mr)
  end
end
