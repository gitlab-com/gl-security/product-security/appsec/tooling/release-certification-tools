require_relative '../scripts/checklist_generator'

RSpec.describe ChecklistGenerator do
  before(:each) do
    allow($stdout).to receive(:puts)
  end

  describe 'finding the current release task' do
    let(:expected_url) { "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues?labels=Monthly%20Release&state=opened&search=Release&in=title&not[labels]=security" }


    it 'finds the current monthly release task' do
      parsed_response = [
        {
          'title' => 'Release 14.0',
          'description' => '#blah\nblah\blah\n\n#22nd: release day'
        },
        {
          'title' => 'Release 14.2.1',
          'description' => '#blah\nblah\blah\n\n#patch release process\nabc'
        }
      ]
      expected_response = double(:httparty_response, parsed_response: parsed_response)
      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)

      response = described_class.find_current_release_task

      expect(response).to eq(parsed_response[0])
    end

    it 'raises when the issue title does not match the expected release task issue title' do
      parsed_response = [
        {
          'title' => 'Release 14.1.1',
          'description' => '#blah\nblah\blah\n\n#22nd: release day\nfor_some_reason_this_has_release_day'
        }
      ]
      expected_response = double(:httparty_response, parsed_response: parsed_response)
      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)

      expect {
        described_class.find_current_release_task
      }.to raise_error(RuntimeError, 'unable to identify release task issue: issue title does not match')
    end

    it 'matches potential monthly release task issue titles' do
      [
        'Release 1.1',
        'Release 1.24',
        'Release 14.0',
        'Release 14.12',
        'Release 100.1',
        'Release 100.12',
        'Release 100.12',
        'Release 100.100',
      ].each do |title|
        expect(described_class.release_task_title_matches?(title)).to eq(true)
      end
    end

    it 'does not match security or patch releases' do
      [
        'Security patch release: 13.12.2, 13.11.5, 13.10.5',
        'Security patch release: 13.14',
        'Security patch Release 13.15',
        'Release 1.2.3',
        'Release 14.1.2',
        'Release 100.1.1'
      ].each do |title|
        expect(described_class.release_task_title_matches?(title)).to eq(false)
      end
    end
  end

  describe 'retrieving jihu contributions' do
    it 'retrieves JiHu contributions for a specific project using the project_id, label, and milestone title' do
      project_id = 5
      milestone_title = '13.5'
      expected_url = "#{API_URL}/projects/#{project_id}/merge_requests?labels=#{JIHU_LABEL_NAME}&milestone=#{milestone_title}&state=merged"
      parsed_response = [
        { 'title' => 'Some MR Title' },
        { 'title' => 'Another MR Title' }
      ]
      expected_response = double(:httparty_response, parsed_response: parsed_response)
      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)

      response = described_class.request_jihu_contributions_for_project(project_id, milestone_title)

      expect(response.length).to eq(2)
    end

    it 'retrieves JiHu contributions for all potential projects and creates a hash' do
      milestone_title = '13.5'

      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests?labels=#{JIHU_LABEL_NAME}&milestone=#{milestone_title}&state=merged"
        parsed_response = [
          { 'title' => "Some MR Title for project_id #{project_id}" },
          { 'title' => "Another MR Title for project_id #{project_id}" }
        ]
        expected_response = double(:httparty_response, parsed_response: parsed_response)
        expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)
      end

      result = described_class.retrieve_jihu_contributions_for(milestone_title)

      expect(result.keys.length).to eq(ALL_PROJECT_IDS.length)

      ALL_PROJECT_IDS.each do |project_id|
        expect(result[project_id].length).to eq(2)
        expect(result[project_id][0]['title']).to eq("Some MR Title for project_id #{project_id}")
        expect(result[project_id][1]['title']).to eq("Another MR Title for project_id #{project_id}")
      end
    end

    it 'determines the contribution count' do
      contributions = {
        'abc' => [{}, {}, {}],
        'def' => [{}, {}, {}],
        'xyz' => [],
        'www' => [],
        '123' => []
      }
      expect(described_class.determine_contribution_count(contributions)).to eq(6)
    end
  end

  describe 'creating the certification issue' do
    let(:jihu_contributions) do
      {
        GITLAB_PROJECT_ID => [
          {
            'title'   => 'Some merged MR',
            'web_url' => 'http://gitlab.example.com/example-one',
            'state'   => 'merged',
            'labels'  => []
          },
          {
            'title'   => 'Some open MR',
            'web_url' => 'http://gitlab.example.com/example-two',
            'state'   => 'open',
            'labels'  => []
          },
          {
            'title'   => 'Some closed MR',
            'web_url' => 'http://gitlab.example.com/example-three',
            'state'   => 'closed',
            'labels'  => []
          }
        ],
        GITALY_PROJECT_ID => [
          {
            'title'   => 'Some customers merged MR',
            'web_url' => 'http://gitlab.example.com/customers-example-one',
            'state'   => 'merged',
            'labels'  => []
          },
          {
            'title'   => 'Some customers open MR',
            'web_url' => 'http://gitlab.example.com/customers-example-two',
            'state'   => 'open',
            'labels'  => []
          },
          {
            'title'   => 'Some customers closed MR',
            'web_url' => 'http://gitlab.example.com/customers-example-three',
            'state'   => 'closed',
            'labels'  => []
          }
        ],
        CUSTOMERS_PROJECT_ID => [],
        VERSION_PROJECT_ID => [],
        OMNIBUS_PROJECT_ID => [],
        ENVIRONMENT_TOOLKIT_PROJECT_ID => [],
        CNG_PROJECT_ID => [],
        CHARTS_PROJECT_ID => [],
        WWW_GITLAB_COM_PROJECT_ID => [],
        DOCS_PROJECT_ID => [],
        RUNNER_PROJECT_ID => [],
        GITLAB_SVG_PROJECT_ID => [],
        GITLAB_QA_PROJECT_ID => []
      }
    end
    let(:milestone_title) { '14.5' }
    let(:milestone_id) { 12341 }
    let(:milestone) do
      {
        'title' => milestone_title,
        'id' => milestone_id
      }
    end
    let(:release_task_url) { 'https://gitlab.example.com/some/task/url' }
    let(:current_user_id) { 6789 }
    let(:user_response) { { 'id' => current_user_id } }
    let(:current_user) { double(:user_response_object, parsed_response: user_response) }

    it 'properly creates a release certification issue and then updates the description' do
      issue_iid = 555
      web_url = 'https://gitlab.example.com'
      expected_url = "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues"
      expected_description = described_class.build_description(jihu_contributions, web_url)

      response_object = {
        'web_url' => web_url,
        'iid' => issue_iid,
        'title' => 'Release 14.1',
        'milestone' => { 'id' => milestone_id }
      }
      expected_response = double(:httparty_response, parsed_response: response_object)

      expected_body = {
        'title' => "AppSec Release Certification for #{response_object['title']}",
        'milestone_id' => milestone_id,
        'description' => expected_description
      }

      #expect(Utils).to receive(:retrieve_current_user).and_return(current_user)
      expect(Utils).to receive(:create_release_certification_issue_params).and_return(expected_body)
      expect(HTTParty).to receive(:post).with(expected_url, headers: HEADERS.merge({'Content-Type' => 'application/json'}), body: expected_body.to_json).and_return(expected_response)
      expect(Utils).to receive(:append_data_to_issue_description)

      expect do
        described_class.create_release_certification_issue(response_object, jihu_contributions)
      end.to output("Issue created at #{response_object['web_url']}. Finalizing issue description...\n...done. All steps complete!\n").to_stdout
    end

    it 'includes each identified JiHu contribution in the release certification issue description' do
      built_description = described_class.build_description(jihu_contributions, release_task_url)

      jihu_contributions[GITLAB_PROJECT_ID].each do |c|
        expect(built_description).to include("- [ ] (#{c['state']}) [#{c['title']}](#{c['web_url']}) **|** :x: **security review label not present**\n")
      end
      jihu_contributions[GITALY_PROJECT_ID].each do |c|
        expect(built_description).to include("- [ ] (#{c['state']}) [#{c['title']}](#{c['web_url']}) **|** :x: **security review label not present**\n")
      end
      expect(built_description.scan("#{NO_CONTRIBUTIONS_MESSAGE}").size).to eq(10)
    end

    it 'includes the release task url in the issue description' do
      built_description = described_class.build_description(jihu_contributions, release_task_url)

      expect(built_description).to include("[release task issue](#{release_task_url})")
    end

    it 'includes a section for projects that need manual review' do
      built_description = described_class.build_description(jihu_contributions, release_task_url)

      expect(built_description).to include(MANUAL_REVIEW_HEADER)
      PRIVATE_PROJECT_IDS.each do |project_id|
        expect(built_description).to include(PROJECT_DISPLAY_NAME_FOR[project_id])
        expect(built_description).to include(PROJECT_NOTE_FOR[project_id]) if PROJECT_NOTE_FOR[project_id]
      end
      expect(built_description.scan("**(Note:").size).to eq(PROJECT_NOTE_FOR.keys.length)
      expect(built_description.scan(MANUAL_REVIEW_DEFAULT_CHECKBOX).size).to eq(PRIVATE_PROJECT_IDS.length)
      expect(built_description.scan(MANUAL_REVIEW_NO_MERGE_REQUESTS_CHECKBOX).size).to eq(PRIVATE_PROJECT_IDS.length)
    end
  end
end
