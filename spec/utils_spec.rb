# frozen_string_literal: true

require_relative '../scripts/utils'

RSpec.describe Utils do
  it 'retrieves user information' do
    expected_url = "#{API_URL}/user"

    response = double(:httparty_response, parsed_response: {})
    expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(response)

    described_class.retrieve_current_user
  end

  describe 'certification checklist creation' do
    let(:issue_description) { 'some issue description' }
    let(:certification_issue_url) { 'https://gitlab.example.com/some/cert/url' }

    it 'generates the certification checklist issue header' do
      url = 'https://gitlab.example.com/some-project/tasks/1234'

      result = described_class.generate_certification_checklist_issue_header(url)
      expect(result.include?('## AppSec Release Certification')).to eq(true)
      expect(result.include?("the [release task issue](#{url})")).to eq(true)
      expect(result.include?('## JiHu Contributions')).to eq(true)
      expect(result.include?("cc #{FEDERAL_APPSEC_TEAM_ALIAS}")).to eq(true)
    end

    it 'creates release certification issue params' do
      task_title = 'Release 14.0.5'
      milestone_id = '14.1'
      release_task = {
        'title' => task_title,
        'milestone' => { 'id' => milestone_id },
      }
      current_user = nil #{ 'id' => 1234 }

      result = described_class.create_release_certification_issue_params(release_task, issue_description, current_user)

      expect(result['title']).to eq("AppSec Release Certification for #{task_title}")
      expect(result['milestone_id']).to eq(milestone_id)
      #expect(result['assignee_id']).to eq(current_user['id'])
      expect(result['description']).to eq(issue_description)
    end

    it 'updates the issue description' do
      result = described_class.update_issue_description(issue_description, certification_issue_url)

      expect(result.include?(issue_description)).to eq(true)
      expect(result.include?('## Certification Comment')).to eq(true)
      expect(result.include?("through [a review](#{certification_issue_url})")).to eq(true)
      expect(result.include?("as part of the [release certification process](#{RELEASE_CERTIFICATION_DOCUMENTATION_URL})")).to eq(true)
    end

    it 'appends data to the issue description' do
      issue_iid = 333
      url = "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues/#{issue_iid}"
      expected_new_description = described_class.update_issue_description(issue_description, certification_issue_url)
      expected_query = { 'description' => expected_new_description }

      expect(HTTParty).to receive(:put).with(url, headers: HEADERS.merge({ 'Content-Type' => 'application/json'}), body: expected_query.to_json)

      described_class.append_data_to_issue_description(issue_iid, certification_issue_url, issue_description)
    end
  end

  describe 'generating labels to be displayed' do
    it 'returns the properly formatted label string when the SECURITY_REVIEW_COMPLETE_LABEL_NAME is present' do
      label_added = true
      expect(described_class.generate_label_display(label_added)).to eq ("~\"#{SECURITY_REVIEW_COMPLETE_LABEL_NAME}\"")
    end

    it 'returns an empty string if the SECURITY_REVIEW_COMPLETE_LABEL_NAME is not present' do
      label_added = false
      expect(described_class.generate_label_display(label_added)).to eq (':x: **security review label not present**')
    end
  end

  describe 'building contribution display' do
    let(:title) { 'some title' }
    let(:web_url) { 'url_here' }
    let(:state) { 'merged' }
    let(:contribution) do
      {
        'title' =>  title,
        'web_url' => web_url,
        'state' => state,
        'labels' => []
      }
    end

    it 'does not pre-check the checkbox for contributions missing the review label' do
      label_added = contribution['labels'].include?(SECURITY_REVIEW_COMPLETE_LABEL_NAME)
      label_display = described_class.generate_label_display(label_added)
      result = described_class.build_contribution_display(contribution)

      expect(result).to eq("- [ ] (#{state}) [#{title}](#{web_url}) **|** #{label_display}\n")
    end

    it 'includes the SECURITY_REVIEW_COMPLETE_LABEL_NAME label if present in the contribution' do
      contribution['labels'] = ['testing', SECURITY_REVIEW_COMPLETE_LABEL_NAME, 'other_label']

      result = described_class.build_contribution_display(contribution)

      expect(result).to eq("- [x] (#{state}) [#{title}](#{web_url}) **|** ~\"#{SECURITY_REVIEW_COMPLETE_LABEL_NAME}\"\n")
    end
  end

  describe '#title_matches_patch_release' do
    it 'titles with X.Y.Z are considered patch releases' do
      patch_release_titles = [
        'Release 14.8.4',
        'Release 1.12.8',
        'Release 1.2.13',
        'Release 12.13.14',
        'Release 111.222.333'
      ]
      patch_release_titles. each do |title|
        expect(described_class.title_matches_patch_release(title)).to be_truthy
      end
    end

    it 'other title combinations are not patch releases' do
      non_patch_release_titles = [
        'Release 14.9',
        'Release 42.2',
        'Release 1.14',
        'Release 1.14 Other things.',
        'Backport bugfix for blah',
        'Things.Or.Stuff',
        'Some. Title. With Three Periods.',
        '1Thing.2Stuff.3Other'
      ]

      non_patch_release_titles.each do |title|
        expect(described_class.title_matches_patch_release(title)).to be_falsey
      end
    end
  end
end
