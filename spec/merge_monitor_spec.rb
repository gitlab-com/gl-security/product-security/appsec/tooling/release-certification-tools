# frozen_string_literal: true

require_relative '../scripts/merge_monitor'
require 'webmock/rspec'

RSpec.describe MergeMonitor do

  let(:request_headers){{
    'Accept'=>'*/*',
    'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
    'Content-Type'=>'application/json',
    'User-Agent'=>'Ruby'
  }}
  before(:each) do
    allow($stdout).to receive(:puts)
  end

  it 'has the expected request params to find MRs that are missing the label' do
    params = described_class.request_params

    expect(params['state']).to eq('merged')
    expect(params['labels']).to eq('JiHu contribution')
    expect(params['not']).to eq({
      'labels' => [SECURITY_REVIEW_COMPLETE_LABEL_NAME]
    })
  end

  describe 'iterating through public projects that JiHu contributes to' do
    let(:report_issues_url) { "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues?state=opened" }

    before(:each) do
      stub_request(:get, "https://gitlab.com/api/v4/projects/34358474/issues?state=opened").
        with(
          headers: request_headers).
        to_return(status: 200, body: '[]', headers: {})
    end
    it 'checks all public projects based on the ALL_PROJECT_IDS constant' do
      expected_urls = ALL_PROJECT_IDS.map do |project_id|
        "#{API_URL}/projects/#{project_id}/merge_requests?labels=JiHu%20contribution&not%5Blabels%5D%5B0%5D=sec-planning::complete&state=merged"
      end
      expected_urls.each do |url|
        stub_request(:get, url).to_return(status: 200, body: "{}", headers: {})
      end
      described_class.check_merged
      expected_urls.each do |url|
        expect(WebMock).to have_requested(:get, url).once
      end
    end

    it 'returns an empty array if there are no findings' do
      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"
        stub_request(:get, expected_url)
          .with(
            headers: request_headers,
            query:described_class.request_params
          ).to_return(status: 200, body: '[]', headers: {})
      end
      expect(described_class.check_merged).to eq([])
    end

    it 'returns an array of each finding' do
      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"
        stub_request(:get, expected_url)
          .with(
            headers: request_headers,
            query: described_class.request_params
          ).to_return(
          status: 200,
          body: '[{"something": "here"}]',
          headers: { 'Content-Type' => 'application/json' }
        )
      end

      expect(described_class.check_merged.length).to eq(ALL_PROJECT_IDS.length)
    end
  end

  describe 'running the merge monitor' do
    let(:report_issues_url) { "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues?state=opened" }
    let(:merge_request) do
      {
        'project_id' => GITLAB_PROJECT_ID,
        'title' => 'Some Title Here',
        'web_url' => 'https://gitlab.com/gitlab-org/gitlab/issues/1234'
      }
    end
    let(:expected_response) { { 'web_url' => 'some_url_here' } }
    let(:response_double) { double(:httparty_response, parsed_response: expected_response) }
    let(:existing_issue_id) { 1234 }
    let(:known_merge_request_id) { 5678 }
    let(:mr_iid) { 9876 }
    let(:related_merge_request) do
      {
        'title' => 'some merge request title',
        'id' => known_merge_request_id,
        'iid' => mr_iid
      }
    end
    let(:related_merge_request_url) { "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues/#{existing_issue_id}/related_merge_requests" }
    let(:response_content){[]}
    let(:mock_response) { instance_double('HTTParty::Response', code: 200, parsed_response: response_content,body:response_content)}

    it 'creates the expected issue request body' do
      second_mr = {
        'project_id' => DOCS_PROJECT_ID,
        'title' => 'Another Title Here',
        'web_url' => 'https://gitlab.com/gitlab-org/gitlab/issues/6789'
      }
      merge_requests = [merge_request, second_mr]

      request_body = described_class.create_request_body(merge_requests)

      expect(request_body['title']).to include('Merge Monitor Report for')
      expect(request_body['description']).to include("## Merged JiHu MRs without the #{SECURITY_REVIEW_COMPLETE_LABEL_NAME} label")
      expect(request_body['description']).to include("* (#{PROJECT_NAME_FOR[merge_request['project_id']]}) [#{merge_request['title']}](#{merge_request['web_url']})\n")
      expect(request_body['description']).to include("* (#{PROJECT_NAME_FOR[second_mr['project_id']]}) [#{second_mr['title']}](#{second_mr['web_url']})\n")
      expect(request_body['description']).to include("cc #{FEDERAL_APPSEC_TEAM_ALIAS}")
    end

    it 'creates an issue if there were merge requests found' do
      stub_request(:get, report_issues_url)
        .with(headers: request_headers)
        .to_return(status: 200, body: [].to_json, headers: { 'Content-Type' => 'application/json' })

      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"
        stub_request(:get, expected_url)
          .with(
            headers: request_headers,
            query: described_class.request_params
          ).to_return(status: 200, body: [merge_request].to_json, headers: { 'Content-Type' => 'application/json' })
      end

      stub_request(:post, %r{.*})
        .with(headers: request_headers)
        .to_return(status: 201, body: response_double.to_json, headers: { 'Content-Type' => 'application/json' })

      described_class.run

      expect(WebMock).to have_requested(:post, %r{.*})
                           .with(headers: request_headers)
                           .once
    end

    it 'does not create an issue if there were no merge requests found' do
      stub_request(:get, report_issues_url)
        .with(headers: request_headers)
        .to_return(status: 200, body: [].to_json, headers: { 'Content-Type' => 'application/json' })

      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"
        stub_request(:get, expected_url)
          .with(
            headers: request_headers,
            query: described_class.request_params
          ).to_return(status: 200, body: [].to_json, headers: { 'Content-Type' => 'application/json' })
      end

      stub_request(:post, %r{.*}).to_return(status: 200)

      described_class.run

      expect(WebMock).not_to have_requested(:post, %r{.*})
    end

    it 'does not create an issue with duplicate merge requests' do
      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"

        if project_id == GITLAB_PROJECT_ID
          stub_request(:get, expected_url)
            .with(headers: request_headers, query: described_class.request_params)
            .to_return(status: 200, body: [related_merge_request].to_json, headers: { 'Content-Type' => 'application/json' })
        else
          stub_request(:get, expected_url)
            .with(headers: request_headers, query: described_class.request_params)
            .to_return(status: 200, body: [].to_json, headers: { 'Content-Type' => 'application/json' })
        end
      end

      stub_request(:get, report_issues_url)
        .with(headers: request_headers)
        .to_return(status: 200, body: [{ 'iid' => existing_issue_id }].to_json, headers: { 'Content-Type' => 'application/json' })

      stub_request(:get, related_merge_request_url)
        .with(headers: request_headers)
        .to_return(status: 200, body: [related_merge_request].to_json, headers: { 'Content-Type' => 'application/json' })

      stub_request(:post, %r{.*}).to_return(status: 200)

      described_class.run

      expect(WebMock).not_to have_requested(:post, %r{.*})
    end

    it 'creates an issue only containing previously unreported merge requests' do
      unknown_merge_request_id = 1111
      previously_unknown_merge_request = {
        'title' => 'this was an unknown merge request',
        'id' => unknown_merge_request_id
      }

      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"

        if project_id == GITLAB_PROJECT_ID
          stub_request(:get, expected_url)
            .with(
              headers: request_headers,
              query: described_class.request_params
            ).to_return(
            status: 200,
            body: [related_merge_request, previously_unknown_merge_request].to_json,
            headers: { 'Content-Type' => 'application/json' }
          )
        else
          stub_request(:get, expected_url)
            .with(
              headers: request_headers,
              query: described_class.request_params
            ).to_return(
            status: 200,
            body: [].to_json,
            headers: { 'Content-Type' => 'application/json' }
          )
        end
      end

      stub_request(:get, report_issues_url)
        .with(headers: request_headers)
        .to_return(
          status: 200,
          body: [{ 'iid' => existing_issue_id }].to_json,
          headers: { 'Content-Type' => 'application/json' }
        )

      stub_request(:get, related_merge_request_url)
        .with(headers: request_headers)
        .to_return(
          status: 200,
          body: [related_merge_request].to_json,
          headers: { 'Content-Type' => 'application/json' }
        )

      expected_body = described_class.create_request_body([previously_unknown_merge_request]).to_json
      stub_request(:post, "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues")
        .with(
          headers: request_headers,
          body: expected_body
        ).to_return(
        status: 201,
        body: response_double.to_json,
        headers: { 'Content-Type' => 'application/json' }
      )

      described_class.run

      ALL_PROJECT_IDS.each do |project_id|
        expected_url = "#{API_URL}/projects/#{project_id}/merge_requests"
        expect(WebMock).to have_requested(:get, expected_url)
                             .with(headers: request_headers, query:described_class.request_params)
                             .once
      end

      expect(WebMock).to have_requested(:get, report_issues_url)
                           .with(headers: request_headers)
                           .once

      expect(WebMock).to have_requested(:get, related_merge_request_url)
                           .with(headers: request_headers)
                           .once

      expect(WebMock).to have_requested(:post, "#{API_URL}/projects/#{MERGE_MONITOR_REPORT_PROJECT_ID}/issues")
                           .with(
                             headers: request_headers,
                             body: expected_body
                           ).once
    end
  end
end
