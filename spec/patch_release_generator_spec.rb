# frozen_string_literal: true

require_relative '../scripts/patch_release_generator'

RSpec.describe PatchReleaseGenerator do
  before(:each) do
    allow($stdout).to receive(:puts)
  end

  let(:mr_id_1) { '63974' }
  let(:mr_id_2) { '62053' }
  let(:mr_id_3) { '88888' }
  let(:jihu_mr_id) { '1234' }
  let(:gitlab_mr_links) do
    [
      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/#{mr_id_1}",
      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/#{mr_id_2}",
      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/#{mr_id_3}",
      "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/#{jihu_mr_id}"
    ]
  end
  let(:gitlab_note_body) do
    "The following merge requests were picked into #{gitlab_mr_links[0]}:\n\n```\n* [SomeNotJiHuExampleOne](#{gitlab_mr_links[1]})\n* [SomeNotJiHuExampleTwo](#{gitlab_mr_links[2]})\n* [Some JiHu Contribution](#{gitlab_mr_links[3]})\n```"
  end
  describe 'identifying jihu contributions from notes' do
    let(:task_id) { 9876 }
    let(:mr_id_4) { '444444' }
    let(:mr_id_5) { '555555' }
    let(:mr_id_6) { '611116' }
    let(:mr_id_7) { '711117' }
    let(:omnibus_mr_links) do
      [
        "https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/#{mr_id_4}",
        "https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/#{mr_id_5}",
      ]
    end
    let(:cng_mr_links) do
      [
        "https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/#{mr_id_6}",
        "https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/#{mr_id_7}",
      ]
    end
    let(:omnibus_note_body) do
      "The following merge requests were picked into #{omnibus_mr_links[0]}:\n\n```\n* [Example Omnibus MR Name here](#{omnibus_mr_links[1]})\n```"
    end
    let(:cng_note_body) do
      "The following merge requests were picked into #{cng_mr_links[0]}:\n\n```\n* [Example CNG MR Name here](#{cng_mr_links[1]})\n```"
    end
    let(:expected_gitlab_ids) do
      [
        mr_id_1,
        mr_id_2,
        mr_id_3,
        jihu_mr_id
      ]
    end
    let(:expected_omnibus_ids) { [mr_id_4, mr_id_5] }
    let(:expected_cng_ids) { [mr_id_6, mr_id_7] }

    it 'identifies release bot comments with merge request lists when there is a single bot comment' do
      bot_comment = {
        'body' => gitlab_note_body
      }
      comments = [
        {
          'notes' => [{ 'body' => 'abc' }]
        },
        {
          'notes' => [bot_comment]
        },
        {
          'notes' => [{ 'body' => 'xyz' }]
        },
      ]
      expected_response = double(:httparty_response, parsed_response: comments)
      expected_url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues/#{task_id}/discussions?per_page=100"

      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)

      result = described_class.retrieve_merge_requests_notes(task_id)

      expect(result.length).to eq(1)
      expect(result[0]).to eq(bot_comment)
    end

    it 'identifies release bot comments with merge request lists when there are multiple bot comments' do
      first_bot_comment = { 'body' => gitlab_note_body }
      second_bot_comment = { 'body' => omnibus_note_body }
      comments = [
        {
          'notes' => [first_bot_comment]
        },
        {
          'notes' => [second_bot_comment]
        },
        {
          'notes' => [{ 'body' => 'xyz' }]
        },
      ]
      expected_response = double(:httparty_response, parsed_response: comments)
      expected_url = "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues/#{task_id}/discussions?per_page=100"

      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)

      result = described_class.retrieve_merge_requests_notes(task_id)

      expect(result.length).to eq(2)
      expect(result.include?(first_bot_comment)).to eq(true)
      expect(result.include?(second_bot_comment)).to eq(true)
    end

    it 'sorts gitlab, omnibus and repostiory notes into a mapping when all are present' do
      notes = [
        { 'body' => gitlab_note_body },
        { 'body' =>  omnibus_note_body },
        { 'body' => cng_note_body }
      ]

      result = described_class.sort_merge_request_notes_by_repository(notes)

      expect(result[:gitlab][:note]).to eq(expected_gitlab_ids)
      expect(result[:omnibus][:note]).to eq(expected_omnibus_ids)
      expect(result[:cng][:note]).to eq(expected_cng_ids)
    end

    it 'sorts gitlab and omnibus repostiory notes into a mapping when just gitlab is present' do
      notes = [{ 'body' => gitlab_note_body }]

      result = described_class.sort_merge_request_notes_by_repository(notes)

      expect(result[:gitlab][:note]).to eq(expected_gitlab_ids)
      expect(result[:omnibus][:note]).to eq([])
      expect(result[:cng][:note]).to eq([])
    end

    it 'sorts gitlab and omnibus repository notes into a mapping when just omnibus is present' do
      notes = [{ 'body' =>  omnibus_note_body }]

      result = described_class.sort_merge_request_notes_by_repository(notes)

      expect(result[:omnibus][:note]).to eq(expected_omnibus_ids)
      expect(result[:gitlab][:note]).to eq([])
    end

    describe 'retrieving jihu contributions' do
      let(:non_jihu_contribution_id) { '123' }
      let(:non_jihu) { { 'labels' => ['bug', 'severity::2', 'group::blah'] } }
      let(:jihu_contribution_id) { '456' }
      let(:jihu_mr) { { 'labels' => ['feature', 'severity::1', 'JiHu contribution'] } }

      it 'does nothing if the array of merge request ids is empty' do
        expect(HTTParty).not_to receive(:get)

        described_class.retrieve_all_jihu_contributions([], GITLAB_PROJECT_ID)
      end

      it 'makes one request per repository' do
        expected_url =  "#{API_URL}/projects/#{GITLAB_PROJECT_ID}/merge_requests?iids[]=#{non_jihu_contribution_id}&iids[]=#{jihu_contribution_id}"

        response = [non_jihu, jihu_mr]
        expected_response = double(:httparty_response, parsed_response: response)
        result = expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(expected_response)

        result = described_class.retrieve_all_jihu_contributions([non_jihu_contribution_id, jihu_contribution_id], GITLAB_PROJECT_ID)


        expect(result.include?(jihu_mr)).to eq(true)
        expect(result.include?(non_jihu)).to eq(false)
      end
    end
  end

  describe 'display contribution counts' do
    let(:gitlab_jihu_contributions) do
      [
        { 'title': 'some JiHu Contributions' },
        { 'title': 'some other JiHu Contributions' },
      ]
    end
    let(:omnibus_jihu_contributions) do
      [
        { 'title': 'yet another JiHu Contributions' },
      ]
    end

    it 'displays jihu contribution count when one repository has them' do
      jihu_contributions = {
        GITLAB_PROJECT_ID  => [],
        OMNIBUS_PROJECT_ID => omnibus_jihu_contributions
      }
      expected_count = 1

      expect do
        described_class.display_contribution_results(jihu_contributions).to output("... #{expected_count} JiHu Contributions found. Please certify the release").to_stdout
      end
    end

    it 'displays jihu contribution count when both repositories have them' do
      jihu_contributions = {
        GITLAB_PROJECT_ID  => gitlab_jihu_contributions,
        OMNIBUS_PROJECT_ID => omnibus_jihu_contributions
      }
      expected_count = 3

      expect do
        described_class.display_contribution_results(jihu_contributions).to output("... #{expected_count} JiHu Contributions found. Please certify the release").to_stdout
      end
    end

    it 'indicates that a patch release does not include jihu contributions' do
      jihu_contributions = {
        GITLAB_PROJECT_ID  => [],
        OMNIBUS_PROJECT_ID => []
      }

      expect do
        described_class.display_contribution_results(jihu_contributions).to output('... no JiHu contributions found. Release certification may not be necessary.').to_stdout
      end
    end
  end

  describe 'running from a scheduled pipeline' do
    let(:task_project_issues_url) { "#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues" }
    let(:patch_release_issue_query) { described_class.find_patch_release_issue_request_query }
    let(:certification_project_issues_url) { "#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues" }
    let(:certification_issues_query) { described_class.recent_certification_issues_query }

    let(:task_iid) { 5555 }
    let(:first_patch_issue_title) { 'Release 1.2.3' }
    let(:patch_release_issue_1) do
      {
        'iid' => task_iid,
        'title' => first_patch_issue_title
      }
    end

    it 'performs the correct actions when the from_automation option is true' do
      options = { from_automation: true }

      blog_mr_iid = 1234
      blog_mr = [{'title'=>'Adding blog post for XX.XX.XX','iid'=>blog_mr_iid}]
      merge_request_url = "https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6338"
      changes_mr = [{'diff' =>"blah blah \n\n[Updating blah](#{merge_request_url})"}]
      non_jihu_contribution_mr = {'labels'=>['dev::workflow']}
      related_mrs_double = double(:response, parsed_response: blog_mr,success?:true,headers:{'X-Next-Page'=>1})
      mr_response_double = double(:mr_response, parsed_response: changes_mr,success?:true,headers:{'X-Next-Page'=>1})
      non_jihu_mr_response = double(:mr_response,parsed_response:non_jihu_contribution_mr,success?:true,headers:{'X-Next-Page'=>1})
      post_response_double = double(:post_response, parsed_response: {})

      expect(HTTParty).to receive(:get).with(task_project_issues_url, headers: HEADERS, query: patch_release_issue_query).and_return([patch_release_issue_1])
      expect(Utils).to receive(:title_matches_patch_release).with(first_patch_issue_title).and_return(true)
      expect(HTTParty).to receive(:get).with(certification_project_issues_url, headers: HEADERS, query: certification_issues_query).and_return([])

      expect(HTTParty).to receive(:get).with("#{API_URL}/projects/#{RELEASE_TASK_PROJECT_ID}/issues/#{task_iid}/related_merge_requests?page=1", headers: HEADERS).and_return(related_mrs_double)
      expected_url = "#{API_URL}/projects/#{WWW_GITLAB_COM_PROJECT_ID}/merge_requests/#{blog_mr_iid}/diffs?page=1"
      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(mr_response_double)
      expected_url = "#{API_URL}/projects/#{GITALY_PROJECT_ID}/merge_requests/6338"
      expect(HTTParty).to receive(:get).with(expected_url, headers: HEADERS).and_return(non_jihu_mr_response)

      expected_post_params = {}
      expect(Utils).to receive(:create_release_certification_issue_params).and_return(expected_post_params)
      expect(Utils).to receive(:append_data_to_issue_description)
      expect(HTTParty).to receive(:post).with("#{API_URL}/projects/#{RELEASE_CERTIFICATION_PROJECT_ID}/issues", headers: HEADERS, query: expected_post_params).and_return(post_response_double)

      described_class.create_checklist(options)
    end
  end
end
